import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  entries = [
    {
      sender: {
        username: 'mattiaq',
        fullName: 'Mattia Q',
        avatarUrl: 'https://i.imgur.com/UF0j44r.gif'
      },
      providers: [],
      content: 'Vabbe\', comunque alla fine direi che la manifestazione è riuscita, molta gente, di tutti i tipi, clima festoso. Per i contenuti torno domani e mi sento il comizio di Nicola (che oggi ho incrociato un paio di volte ma non sono riuscito a salutare, domani sarà ancora più difficile).',
      images: [
        {source: 'https://i.imgur.com/OqF2vU1.jpg', alt: 'Description for Image 1', title: 'Title 1'},
        {source: 'http://i.imgur.com/2OHsWFc.jpg', alt: 'Description for Image 2', title: 'Title 2'},
        {source: 'http://i.imgur.com/8kz4dpk.jpg', alt: 'Description for Image 3', title: 'Title 3'},
      ],
      comments: [
        {
          sender: {
            username: 'mattiaq',
            fullName: 'mattiaq',
          },
          content: 'L\'area più affollata senza dubbio quella food and beverage. :)'
        },
        {
          sender: {
            username: 'senape',
            fullName: 'senape',
          },
          content: 'Messaggio farlocco di prova, per vedere di nascosto l\'effetto che fa, soprattutto per capire' +
            'cosa significhi avere un commento lungo, lungo, lunghissimo, che dovrebbe, in linea del tutto teorica' +
            'suddividersi su diverse linee.'
        }
      ]
    },
    {
      sender: {
        username: 'senape',
        fullName: 'Senape\'s dan',
        avatarUrl: 'https://pbs.twimg.com/profile_images/33173662/avatar-140x140-140x140_bigger.jpg'
      },
      providers: [],
      content: 'Poto poto poto',
      images: [],
      comments: []
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
